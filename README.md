# Harris-Operator + BRIEF-Descriptor #

This Project implements the Harris-Operator and BRIEF-Descriptor to find Key Points and calculate Descriptors in images.

## Prequesites ##

* **CMake** 2.8+
* **OpenCV** 3.4.4+

## Build ##
This Project supports cmake. 

Building with Linux/MacOS:

```bash
cd /path/to/harris-brief
mkdir build
cd build
cmake ..
make
```
Building with Windows:

```bash
cd /path/to/harris-brief
mkdir build
cd build
cmake ..
```
After creating the \*.sln-File you can build the project with Visual Studio 15.
