#include "FeaturePoint.hpp"
#include "Descriptor.hpp"
#include <random>
#include <ctime>
#include <vector>
#include <opencv2/opencv.hpp>

using namespace cv;
Descriptor::Descriptor(int anzahl, int streuung, Mat bild) {
	this->img = bild;
	this->anzahl = anzahl;

	mt = std::mt19937(rd());
	dist_x = std::uniform_real_distribution<double>(0, bild.cols - 1);
	dist_y = std::uniform_real_distribution<double>(0, bild.rows - 1);

	if (bild.type() != CV_8U) {
		cvtColor(bild, bild, COLOR_RGB2GRAY);
	}

	if (streuung > bild.rows || streuung > bild.cols) {
		this->streuung = min(bild.rows, bild.cols);
	}
	else
	this->streuung = streuung;
}

int Descriptor::binary_test(Point2d p, int anzahl, int streuung, Mat bild) {
	int x;
	int y;
	int brief_descriptor = 0;
	for (int i = 0; i < anzahl; i++) {
		x = (int)dist_x(mt);
		y = (int)dist_y(mt);
		Point2d punkt = Point2d(x, y);

		if (bild.at<uchar>(x, y) > bild.at<uchar>(p.x, p.y)) {
			brief_descriptor += (int)pow(2, i - 1);
		}
	}
	return brief_descriptor;
}

int* Descriptor::allg(std::vector<FeaturePoint> cloud) {
	auto * Brief_cloud = (int *)calloc(cloud.size(), sizeof(int));
	for (int i = 0; i < cloud.size(); i++) {
		Brief_cloud[i] = binary_test(cloud[i].point,this->anzahl,this->streuung, this->img );
	}
	return Brief_cloud;
}
