#include "Detector.hpp"
#include <opencv2/highgui.hpp>

#include <iostream>
#include <math.h>
using namespace std;

// ======== Definition Helper Methods ======== //
inline int index(int r, int c, int cols);

int max_used_val_in_histogram (cv::Mat &mat, bool respect_0);
void find_local_maximas(cv::Mat &src,int window_size, float harris_threshold, cv::Mat &dst, std::vector<FeaturePoint>* out_points);

bool compare_feature_points(FeaturePoint fp1, FeaturePoint fp2);

// ======== Instance Methods ======== //
vector<FeaturePoint> Detector::detect(cv::Mat image){
	int cols = image.cols;
	int rows = image.rows;

	// Apply Gaussian Blur to reduce noise in image
	cv::GaussianBlur(image, image, cv::Size(3, 3), 0, 0);

	vector<FeaturePoint> feature_points(0);

	if (image.type() != CV_8U) throw "Wrong Matrix type. Only CV_8U is allowed";
	if (image.empty()) return feature_points;

	// Initialize theta_value matrix
	cv::Mat theta_values = cv::Mat::zeros(rows, cols, CV_32F);
	float alpha = 0.05;

	// Start 1 pixel in and end 1 pixel early to prevent index out of range cases
	int dist = window_size / 2;
	int end_c = cols - dist;
	int end_r = rows - dist;

	uchar* img_data = image.data;

	// Look at every pixel and surrounding area to find possible features
    for (int r = dist + 1; r < end_r; r++){
        for (int c = dist + 1; c < end_c; c++){

        	// Initialize values of M
			float m11 = 0, m12 = 0, m21 = 0, m22 = 0;

			if(dist == 0){
                int grad_x = image.at<uchar>(r, c + 1) - image.at<uchar>(r, c - 1);
                int grad_y = image.at<uchar>(r + 1, c) - image.at<uchar>(r - 1, c);

                m11 = grad_x * grad_x;
                m21 = m12 = grad_x * grad_y;
                m22 = grad_y * grad_y;

                float theta = ((m11 * m22 - m12 * m21) - alpha * (m11 + m22) * (m11 + m22));

                theta_values.at<float>(r, c) = theta;
            } else {
                for (int y = -dist; y <= dist; y++) {
                    for (int x = -dist; x <= dist; x++) {
                        int wx = c + x;
                        int wy = r + y;

                        // Calculate x- and y-Gradient at position wx|wy
                        int grad_x = image.at<uchar>(wy, wx + 1) - image.at<uchar>(wy, wx - 1);
                        int grad_y = image.at<uchar>(wy + 1, wx) - image.at<uchar>(wy - 1, wx);

                        // Calculate addend of sum to get M
                        float mi11, mi12, mi21, mi22;

                        mi11 = grad_x * grad_x;
                        mi21 = mi12 = grad_x * grad_y;
                        mi22 = grad_y * grad_y;

                        // Sum up M_i to M
                        m11 += mi11;
                        m12 += mi12;
                        m21 += mi21;
                        m22 += mi22;
                    }
                }

                // Calculate quality value theta of pixel
                // theta = det(M) - alpha * (trace(M))^2
                float theta = ((m11 * m22 - m12 * m21) - alpha * (m11 + m22) * (m11 + m22));
                // If theta is greater than zero, it means there is a corner. Only save corners.
                if(theta > 0) {
                    theta_values.at<float>(r - dist, c - dist) = theta;
                }
            }

        }
	}

	// Normalize theta values to be within range of a uchar matrix
	cv::Mat normalized_theta;
	cv::normalize (theta_values, normalized_theta, 0, 255, cv::NORM_MINMAX);
	normalized_theta.convertTo(normalized_theta, CV_8U);

	//cv::imwrite("theta_" + std::to_string(window_size) + ".png", theta_values);

	// Find local maximas
    cv::Mat max_loc_image(rows, cols, CV_8U);
	find_local_maximas(normalized_theta, this->window_size, this->harris_threshold, max_loc_image, &feature_points);

	if(this->max_features > -1 && feature_points.size()>this->max_features) {
		sort(feature_points.begin(), feature_points.end(), compare_feature_points);
		feature_points.resize(static_cast<unsigned long>(this->max_features));
	}

	return feature_points;
}

// ======== Implementation Helper Methods ======== //

inline int index(int r, int c, int cols){
    return c + r * cols;
}

void find_local_maximas(cv::Mat &src,int window_size, float harris_threshold, cv::Mat &dst, std::vector<FeaturePoint>* out_points) {
	if (/*window_size < 3 || */window_size % 2 == 0)
	{
		dst = src.clone();
		return;
	}

	cv::Mat m0;
	dst = src.clone();
	cv::Point max_loc(0,0);

	// Be sure to have at least 3x3 for at least looking at 1 pixel close neighbours
	int window_center = (window_size-1)/2;

	// Create the localWindow mask to get things done faster
	// When there is a local maxima we will multiply the subwindow with this MASK
	// So that we will not search for those 0 values again
	cv::Mat local_window_mask = cv::Mat::zeros(cv::Size(window_size,window_size),CV_8U);//boolean
	local_window_mask.at<unsigned char>(window_center,window_center)=1;

	// Find the threshold value to threshold the image
	// this function here returns the peak of histogram of picture
	// the picture is a thresholded picture it will have a lot of zero values in it
	// so the second boolean variable says :
	// (boolean) ? "return peak even if it is at 0" : "return peak discarding 0"
	int thresh =  max_used_val_in_histogram(dst,false);
	threshold(dst,m0,thresh,1,cv::THRESH_BINARY);

	// Now delete all thresholded values from picture
	dst = dst.mul(m0);

	// put the src in the middle of the big array
	for (int row=window_center;row<dst.size().height - window_center;row++)
        for (int col = window_center; col < dst.size().width - window_center; col++) {
            // if the value is zero it can not be a local maxima
            if (dst.at<unsigned char>(row, col) == 0)
                continue;

            // the value at (row,col) is not 0 so it can be a local maxima point
            m0 = dst.colRange(col - window_center, col + window_center + 1).rowRange(row - window_center, row + window_center + 1);
            minMaxLoc(m0, nullptr, nullptr, nullptr, &max_loc);

            // If the maximum location of this subWindow is at center
            // it means the local maxima is found
            // so it should delete the surrounding values which lie in the subWindow area
            // hence do not not try to find if a neighbour already was
            if ((max_loc.x == window_center) && (max_loc.y == window_center)) {
            	m0 = m0.mul(local_window_mask);

            	uchar value = dst.at<uchar>(row, col);
            	if(value>harris_threshold) {
					cv::Point point(col, row);
					out_points->emplace_back(point, value);
				}
                // skip the values that are already set to 0 by function above
                col += window_center;
            }
        }

}

int max_used_val_in_histogram (cv::Mat &mat, bool respect_0) {
	// Convert image to floating point because calcHist can only work with floating point numbers
	cv::Mat image;
	mat.convertTo(image, CV_32F);

	// Define all variables needed for Histogram calculation
	int img_num = 1; // number of images in image array
	const int* channels = nullptr; // array containing channel indices (nullptr if default)
	cv::Mat mask; // mask to decide which pixels are important (not used)
	cv::MatND hist; // result
	int dimension = 1; // number of dimensions in histogram
	int hist_size = 256; // number of histogram entries
	float range[] = {0, 256}; //range of values in image
	const float* hist_range = {range};

	cv::calcHist(&image, img_num, channels, mask, hist, dimension, &hist_size, &hist_range);

	// Find most used value in histogram
	double max_val = 0;
	cv::Point max_loc;
	cv::minMaxLoc(hist, nullptr, &max_val, nullptr, &max_loc);

	// If most used value is zero, find next most used value
	if(!respect_0 && max_val == 0){
		hist.at<uchar>(max_loc) = 0;
		cv::minMaxLoc(hist, nullptr, nullptr, nullptr, &max_loc);
	}

	return max_loc.y;
}


bool compare_feature_points(FeaturePoint fp1, FeaturePoint fp2){
	return fp1.get_size()>fp2.get_size();
}