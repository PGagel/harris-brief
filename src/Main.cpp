#include "Detector.hpp"
#include "Descriptor.hpp"

#include <opencv2/highgui.hpp>

#include <iostream>
#include <vector>

int main(int argc, char* argv[]){
	//cv::Mat image = cv::imread("/home/philipp/Bilder/Trevi_Fountain.png");
	cv::Mat image = cv::imread("../res/chess.png");

	cv::VideoCapture cap(0);
	cv::Mat gray;
	int window_size = 1;
	cvtColor(image, gray, cv::COLOR_BGR2GRAY);
	try{
		Detector detector (-1, window_size, 10);
		Descriptor descriptor(128, cv::min(gray.cols, gray.rows) / 2, gray);

		while(waitKey(1) != 27) {
            cap >> image;
            cv::cvtColor(image, gray, COLOR_BGR2GRAY);
            std::vector<FeaturePoint> f_points = detector.detect(gray);
            int *descs = descriptor.allg(f_points);

            for (auto &f_point : f_points)
                cv::circle(image, f_point.point, 3, cv::Scalar(255, 0, 0));
            imshow("result", image);
        }
        cv::imwrite("result_" + std::to_string(window_size) + ".png", image);
	} catch (char const* error) {
		std::cout << error << std::endl;
	}
	return 0;
}
