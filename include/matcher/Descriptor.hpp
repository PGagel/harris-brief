#include "FeaturePoint.hpp"
#include <random>
#include <time.h>
#include <opencv2/opencv.hpp>

using namespace cv;



class Descriptor {
private:
	Mat img;
	int streuung;
	int anzahl;

	std::random_device rd;
	std::mt19937 mt;
	std::uniform_real_distribution<double> dist_x;
	std::uniform_real_distribution<double> dist_y;

public:
	Descriptor(int anzahl, int streuung, Mat bild);
	int binary_test(Point2d p, int anzahl, int streuung, Mat bild);
	int* allg(std::vector<FeaturePoint> cloud);
};