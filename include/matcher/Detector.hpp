#pragma once

#include <opencv2/imgproc.hpp>
#include <vector>

#include "FeaturePoint.hpp"

class Detector{
private:
	int max_features;
	int window_size;
	float harris_threshold;
	
public:
	Detector(int max_features, int window_size, float harris_threshold) : max_features(max_features), window_size(window_size),  harris_threshold(harris_threshold) {}

	
	std::vector<FeaturePoint> detect(cv::Mat image);
};
