#pragma once

#include <opencv2/core.hpp>

class FeaturePoint {
private:
	float size;
	float angle;
public:
	FeaturePoint() : point(cv::Point2d(0, 0)), size(0), angle(0) {}
	FeaturePoint(int x, int y) : point(cv::Point2d(x, y)), size(0), angle(0){}
	explicit FeaturePoint(cv::Point& point, float size = 0, float angle = 0) : point(point), size(size), angle(angle){}
	cv::Point point;

	float get_size(){return this->size;}
	float get_angle(){return this->angle;}
};
